from fabric.api import task, local
from fabric.colors import red, green


@task(alias='archive-all')
def archive_all():
    """Creates a gzipped tar file just as git-archive but including all the
    submodules if any. Run this task at root directory of your git repository.
    """
    import os
    import tarfile

    def ls_files(prefix=''):
        """Does a `git ls-files` on every git repository (eg: submodules)
        found in the working git repository and returns a list with all the
        filenames returned by each `git ls-files`"""
        # --full-name Forces paths to be output relative to the project top directory
        # --exclude-standard adds standard git exclusions (.git/info/exclude, .gitignore, ...)
        command = 'git ls-files --full-name --exclude-standard'
        raw_files = local(command, capture=True)
        files = []
        for filename in raw_files.split('\n'):
            if os.path.isdir(filename) and os.path.exists(os.path.join(filename, '.git')):
                os.chdir(filename)
                files.extend(ls_files(prefix=filename))
            else:
                files.append(os.path.join(prefix, filename))
        return files

    cwd = os.getcwd()
    files = ls_files()
    os.chdir(cwd)

    output = '%s.tar.gz' % os.path.basename(cwd)
    project_tar = tarfile.open(output, 'w:gz')
    for filename in files:
        project_tar.add(filename)
    project_tar.close()

    print(green('Archive created at %s' % output))
